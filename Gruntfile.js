/*

Grunt-PHP-browserSync-watch-sass-autoprefixer-cssmin



 */

//file globing
//http://gruntjs.com/configuring-tasks#globbing-patterns
//dir/**/*.js
module.exports = function(grunt) {
    grunt.initConfig({
        dirs: {
            //Defining directories
            //E.G. dirs.css = dev/css access via <%= dirs.css %>
            //DIRECTORIES ARE RELATIVE TO Grunt.js FILE (folder contains Grunt.js and dev directory)
            css: "dev/css",
            scss: "dev/scss",
            js: "dev/js",
            html: "dev/html",
            php: "dev/php",
            somethingelse: "dev/somethingelse"
        },
        watch: { //Watch for changes in files and then run task(s) E.G. tasks:['jslint',...]
        //more info at https://github.com/gruntjs/grunt-contrib-watch#grunt-contrib-watch-v100--
            sass: { //SASS (SCSS syntax) on change run sass-> autoprefixer->cssmin
                files: 'dev/**/*.scss',
                tasks: ['sass', 'autoprefixer', 'cssmin']
            },
            js: { //JavaScript(JS/ES6) on change run task uglify
                files: 'dev/**/*.js',
                tasks: ['uglify']
            },
            html: { //HTML5 on change run task htmlmin
                files: ['dev/**/*.html'],
                tasks: ['htmlmin']
            }
        },
        sass: { //SASS TASK (convert/merge .scss into .css)
          //more info at https://github.com/gruntjs/grunt-contrib-sass#grunt-contrib-sass-v100--
            dev: {
                files: {
                    //create temp css from main.css
                    //dest : source
                    //dev/css/temp.css : dev/scss/main.scss
                    '<%= dirs.css %>/temp.css': '<%= dirs.scss %>/main.scss'
                }
            }
        },
        autoprefixer: { //AUTOPREFIXER TASK (for predefined browser add additional css syntax)
          //more info at https://github.com/postcss/autoprefixer#autoprefixer-
            options: { //browser support options
                browsers: ['ie 9']
            },
            css: {
                src: '<%= dirs.css %>/temp.css',
                dest: '<%= dirs.css %>/temp.css'
            }
        },
        cssmin: {//CSSMIN TASK (minimize .css files)
          //more info at https://github.com/gruntjs/grunt-contrib-cssmin#grunt-contrib-cssmin-v101--
            combine: {
                files: {
                  'deploy/css/main.min.css': ['<%= dirs.css %>/temp.css'],
                  '<%= dirs.css %>/main.min.css': ['<%= dirs.css %>/temp.css']
                },
            },
        },
        uglify: {//UGLIFY TASK ( minimize .js files )
          //more at https://github.com/gruntjs/grunt-contrib-uglify#grunt-contrib-uglify-v101--
            options: {
                mangle: false //IMPORTANT !! for angularJS files
            },
            my_target: {
      files: {
        'deploy/js/main.min.js': ['dev/js/main.js']
      }
    }
        },
        htmlmin: { // HTMLMIN TASK (minimize .html files)
          //more at https://github.com/gruntjs/grunt-contrib-htmlmin#grunt-contrib-htmlmin-v140--
            dist: {
                options: {
                    removeComments: true,
                    collapseWhitespace: true,
                    minifyCSS: true, //minify code inside <style> inside .html ()
                    minifyJS: true //minify code inside <script> inside .html ()
                },
                files: {
                    'deploy/index.html': 'dev/index.html', // 'destination': 'source'

                }
            }
        },
        php: {//PHP TASK (start PHP 5.4.0+ server)
          //more at https://github.com/sindresorhus/grunt-php#grunt-php-
            dist: {
                options: {
                    // hostname: '127.0.0.1',
                    port: 3001, //IMPORTANT !!! check your firewall settings
                    base: 'dev', // Project root directory
                    keepalive: false,
                    open: false
                }
            }
        },
        browserSync: {//TASK BROWSERSYNC (reload browser on file change)
          //more at https://github.com/BrowserSync/grunt-browser-sync#grunt-browser-sync-
            dev: {
                reload: true, //reload browser if specified files below has been changed
                bsFiles: {
                    src: [//watch thease files
                        'dev/**/*.css',
                        'dev/**/*.scss',
                        'dev/**/*.html',
                        'dev/**/*.js',
                        'dev/**/*.php'
                    ]
                },
                options: {
                    watchTask: true,
                    proxy: '127.0.0.1:3001',
                    //PHP server is at localhost on port 3001
                    port: 3001, // IMPORTANT !! check your firewall
                    open: true,
                    //  server: './dev'
                }
            }
        }



    });

    // LOAD NPM TASKS
    grunt.loadNpmTasks('grunt-contrib-sass');
    grunt.loadNpmTasks('grunt-autoprefixer');
    grunt.loadNpmTasks('grunt-contrib-cssmin');
    grunt.loadNpmTasks('grunt-contrib-uglify');
    grunt.loadNpmTasks('grunt-contrib-htmlmin');
    grunt.loadNpmTasks('grunt-browser-sync');
    grunt.loadNpmTasks('grunt-contrib-watch');
    grunt.loadNpmTasks('grunt-php');


    // define default task (default is when we run grunt command in console (CLI))
    //E.G. for Linux mypc@mypc: grunt
    grunt.registerTask('default', ['php', 'browserSync', 'watch']);
};
